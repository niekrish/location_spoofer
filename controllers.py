"""
This file defines actions, i.e. functions the URLs are mapped into
The @action(path) decorator exposed the function at URL:

    http://127.0.0.1:8000/{app_name}/{path}

If app_name == '_default' then simply

    http://127.0.0.1:8000/{path}

If path == 'index' it can be omitted:

    http://127.0.0.1:8000/

The path follows the bottlepy syntax.

@action.uses('generic.html')  indicates that the action uses the generic.html template
@action.uses(session)         indicates that the action uses the session
@action.uses(db)              indicates that the action uses the db
@action.uses(T)               indicates that the action uses the i18n & pluralization
@action.uses(auth.user)       indicates that the action requires a logged in user
@action.uses(auth)            indicates that the action requires the auth object

session, db, T, auth, and tempates are examples of Fixtures.
Warning: Fixtures MUST be declared with @action.uses({fixtures}) else your app will result in undefined behavior
"""
import uuid
import random

from py4web import action, request, abort, redirect, URL
from yatl.helpers import A
from .common import db, session, T, cache, auth, logger, authenticated, unauthenticated, flash
from py4web.utils.url_signer import URLSigner
from .models import get_user_email, get_user
from py4web.utils.form import Form, FormStyleBulma

url_signer = URLSigner(session)

@action('index')
@action.uses(db, auth, 'index.html')
def index():
    return dict(
        # COMPLETE: return here any signed URLs you need.
        load_posts_url=URL('load_posts', signer=url_signer),
        add_post_url=URL('add_post', signer=url_signer),
        delete_post_url=URL('delete_post', signer=url_signer),
        my_callback_url=URL('my_callback', signer=url_signer),
        get_images_url = URL('get_posts', signer=url_signer),
        get_rating_url = URL('get_rating', signer=url_signer),
        set_rating_url = URL('set_rating', signer=url_signer),
        search_url = URL('search', signer=url_signer),

      #  table_info_url = URL('table_info', signer=url_signer),

    )

@action('file_upload', method="PUT")
@action.uses() # Add here things you might want to use.
def file_upload():
    file_name = request.params.get("file_name")
    file_type = request.params.get("file_type")
    uploaded_file = request.body # This is a file, you can read it.
    # Diagnostics
    print("Uploaded", file_name, "of type", file_type)
    print("Content:", uploaded_file.read())
    return "ok"



@action('table_info')
@action.uses(db, auth.user, 'table_info.html')
def table_info():
    rows = db(db.contacts.user_email == get_user_email()).select()
    my_list = db(db.phone.contact_id == db.contacts.id).select().as_list()
    return dict(
                rows=rows,
                url_signer=url_signer,
                my_list=my_list,

               )

@action('upload_page')
@action.uses(db, auth.user, 'upload_page.html')
def upload_page():
    return dict(
                file_upload_url = URL('file_upload', signer=url_signer),
               )

@action('spoofers_table')
@action.uses(db, auth.user, 'spoofers_table.html')
def spoofers_table():
    spoofList = db(db.contacts.player_name).select()
    my_list = db(db.phone.contact_id == db.contacts.id).select().as_list()
    return dict(
                spoofList=spoofList,
                url_signer=url_signer,
                my_list=my_list,

               )

@action('to_spoof')
@action.uses(db, session, auth.user)
def to_evidence():
    redirect(URL('spoofers_table'))
    return dict()

@action('to_upload')
@action.uses(db, session, auth.user)
def to_upload():
    redirect(URL('upload_page'))
    return dict()

@action('add_contact', method = ["GET", "POST"])
@action.uses(db, session, auth.user, 'add_contact.html')
def add_contact():
    form = Form(db.contacts, csrf_session=session, formstyle= FormStyleBulma)
    if form.accepted:
        redirect(URL('table_info'))
    return dict(form=form)

@action('edit_contact/<id>', method=["GET", "POST"])
@action.uses(db, session, auth.user, 'edit_contact.html', url_signer.verify())
def edit_contact(id=None):
    assert id is not None
    p = db.contacts[id]
    if p is None:
        redirect(URL('table_info'))
    form = Form(db.contacts, record=p, deletable=False, csrf_session=session, formstyle= FormStyleBulma)
    if form.accepted:
        redirect(URL('table_info'))

    return dict(form=form,
                url_signer=url_signer,

                )

@action('delete_contact/<id>')
@action.uses(db, session, auth.user, url_signer.verify())
def delete_contact(id=None):
    assert id is not None
    db(db.contacts.id == id).delete()
    redirect(URL('table_info'))


@action('edit_phone/<contact_id:int>')
@action.uses(db, session, auth.user, 'edit_phone.html', url_signer.verify())
def edit_phone(contact_id=None):
    rows2 = db(db.phone.contact_id == contact_id).select()
    p = db.contacts[contact_id]
    name_string = "Evidence Submission for " + p.player_name + "'s Spoofing"
    return dict(rows2=rows2,
                contact_id=contact_id,
                name_string=name_string,
                url_signer=url_signer,
                )

@action('add_phone/<contact_id:int>', method = ["GET", "POST"])
@action.uses(db, session, auth.user, 'add_phone.html')
def add_phone(contact_id=None):
    form = Form(db.phone, csrf_session=session, formstyle= FormStyleBulma, dbio=False)
    if form.accepted:
        db.phone.insert(contact_id=contact_id, evidence_type=form.vars["evidence_type"], evidence_description=form.vars["evidence_description"])
        redirect(URL('edit_phone', contact_id, signer=url_signer))
    return dict(form=form)

@action('back')
@action.uses(db, session, auth.user)
def back():
    redirect(URL('table_info'))
    return dict()

@action('to_home')
@action.uses(db, session, auth.user)
def to_evidence():
    redirect(URL('index'))
    return dict()


@action('edit_phone_numbers/<contact_id>/<phone_id>', method=["GET", "POST"])
@action.uses(db, session, auth.user, 'edit_phone_numbers.html', url_signer.verify())
def edit_phone_numbers(contact_id, phone_id=None):
    assert phone_id is not None
    p = db.phone[phone_id]
    if p is None:
        redirect(URL('edit_phone', contact_id))
    d = db.contacts[contact_id]
    name_string = "Phone Numbers for " + d.location + " " + d.player_name
    form = Form(db.phone, record=p, deletable=False, csrf_session=session, formstyle= FormStyleBulma)
    if form.accepted:
        redirect(URL('edit_phone', p.contact_id, signer=url_signer))
    return dict(form=form,
                url_signer=url_signer,
                name_string=name_string
                )

@action('delete_phone/<phone_id>')
@action.uses(db, session, auth.user, url_signer.verify())
def delete_phone(phone_id=None):
    assert phone_id is not None
    p = db.phone[phone_id]
    db(db.phone.id == phone_id).delete()
    redirect(URL('edit_phone', p.contact_id, signer=url_signer))



@action('search')
@action.uses()
def search():
    q = request.params.get("q")
    results = [q + ":" + str(uuid.uuid1()) for _ in range(random.randint(2, 6))]
    return dict(results=results)

@action('load_posts')
@action.uses(url_signer.verify(), db)
def load_posts():
    rows = db(db.post).select().as_list()
    return dict(rows=rows)


@action('add_post', method=["POST"])
@action.uses(url_signer.verify(), db)
def add_post():
    r = db(db.auth_user.email == get_user_email()).select().first()
    post_id = db.post.insert(
        post_info=request.json.get('post_info'),
        post_user=r.first_name + " " + r.last_name if r is not None else "Unknown",
        email=r.email if r is not None else "Unknown"
    )
    return dict(post_id=post_id)

@action('delete_post')
@action.uses(url_signer.verify(), db)
def delete_post():
    id = request.params.get('id')
    assert id is not None
    db(db.post.id == id).delete()
    return "ok"

@action('get_posts')
@action.uses(url_signer.verify(), db)
def get_posts():
    """Returns the list of images."""
    return dict(posts=db(db.posts).select().as_list())

@action('get_rating')
@action.uses(url_signer.verify(), db, auth.user)
def get_rating():
    """Returns the rating for a user and an image."""
    posts_id = request.params.get('posts_id')
    row = db((db.stars.posts == posts_id) &
             (db.stars.rater == get_user())).select().first()
    rating = row.rating if row is not None else 0
    return dict(rating=rating)

@action('set_rating', method='POST')
@action.uses(url_signer.verify(), db, auth.user)
def set_rating():
    """Sets the rating for an image."""
    posts_id = request.json.get('posts_id')
    rating = request.json.get('rating')
    assert posts_id is not None and rating is not None
    db.stars.update_or_insert(
        ((db.stars.posts == posts_id) & (db.stars.rater == get_user())),
        posts=posts_id,
        rater=get_user(),
        rating=rating
    )
    return "ok" # Just to have some confirmation in the Network tab.



