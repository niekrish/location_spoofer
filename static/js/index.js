// This will be the object that will contain the Vue attributes
// and be used to initialize it.
let app = {};


// Given an empty app object, initializes it filling its attributes,
// creates a Vue instance, and then initializes the Vue instance.
let init = (app) => {

    // This is the Vue data.
    app.data = {
        add_mode: false,
        add_post_info: "",
        add_post_user: "",
        rows: [],
        selection_done: false,
        uploading: false,
        uploaded_file: "",
        uploaded: false,
        img_url: "",
        // Complete as you see fit.
    };

    app.file = null;

    app.enumerate = (a) => {
        // This adds an _idx field to each element of the array.
        let k = 0;
        a.map((e) => {e._idx = k++;});
        return a;
    };

  //  app.complete = (rows) => {
  //      rows.map((post) => {
  //          post.thumbs_up_image = 0;
  //          post.thumbs_down_image = 0;
  //      })
  //  }

        app.select_file = function (event) {
        // Reads the file.
        let input = event.target;
        app.file = input.files[0];
        if (app.file) {
            app.vue.selection_done = true;
            // We read the file.
            let reader = new FileReader();
            reader.addEventListener("load", function () {
                app.vue.img_url = reader.result;
            });
            reader.readAsDataURL(app.file);
        }
    };

    app.upload_complete = function (file_name, file_type) {
        app.vue.uploading = false;
        app.vue.uploaded = true;
    };

    app.upload_file = function () {
        if (app.file) {
            let file_type = app.file.type;
            let file_name = app.file.name;
            let full_url = file_upload_url + "&file_name=" + encodeURIComponent(file_name)
                + "&file_type=" + encodeURIComponent(file_type);
            // Uploads the file, using the low-level streaming interface. This avoid any
            // encoding.
            app.vue.uploading = true;
            let req = new XMLHttpRequest();
            req.addEventListener("load", function () {
                app.upload_complete(file_name, file_type)
            });
            req.open("PUT", full_url, true);
            req.send(app.file);
        }
    };


    app.add_post = function () {
        axios.post(add_post_url,
            {
                post_info: app.vue.add_post_info,
            }).then(function (response) {
            app.vue.rows.push({
                id: response.data.id,
                post_info: app.vue.add_post_info,
                email: response.data.email,
                thumbs_up_image: 0,
                thumbs_down_image: 0,
            });

            app.enumerate(app.vue.rows);
            app.reset_form();
            app.set_add_status(false);
        });
        app.init()
    };

    app.reset_form = function () {
        app.vue.add_post_info = "";
    };

    app.delete_post = function(row_idx) {
        let id = app.vue.rows[row_idx].id;
        axios.get(delete_post_url, {params: {id: id}}).then(function (response) {
            for (let i = 0; i < app.vue.rows.length; i++) {
                if (app.vue.rows[i].id === id) {
                    app.vue.rows.splice(i, 1);
                    app.enumerate(app.vue.rows);
                    break;
                }
            }
            });
    };

    app.set_add_status = function (new_status) {
        app.vue.add_mode = new_status;
    };

    //app.set_thumbs = function(post_idx, thumbs_up, thumbs_down) {
      //  let post = app.vue.rows[post_idx];
        //post.thumbs_up_image = thumbs_up;
       // post.thumbs_down_image = thumbs_down;
        //axios.post(set_thumbs_url, {post_id: post.id, up: thumbs_up, down: thumbs_down});
    //};
    // This contains all the methods.
    app.methods = {
        // Complete as you see fit.
        add_post: app.add_post,
        set_add_status: app.set_add_status,
        delete_post: app.delete_post,
        select_file: app.select_file,
        upload_file: app.upload_file,
    };

    // This creates the Vue instance.
    app.vue = new Vue({
        el: "#vue-target",
        data: app.data,
        methods: app.methods
    });

    // And this initializes it.
    app.init = () => {
        // Put here any initialization code.
        // Typically this is a server GET call to load the data.
        axios.get(load_posts_url).then(function (response) {
            app.vue.rows = app.enumerate(response.data.rows);
        });
    };




    // Call to the initializer.
    app.init();
};

// This takes the (empty) app object, and initializes it,
// putting all the code i
init(app);
